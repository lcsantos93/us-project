

<?php 
  $items = array("red", "green", "blue", "yellow"); 

  foreach ($colors as $value) {
      echo "$value <br>";
  }
?>   


<?php
  class Component {
    public $name = "";
    public $description  = "";
    //public $date = "";
  }

  $component = new Component();
  $component->name = "foo";
  $component->description  = "bar";

  // Returns: {"firstname":"foo","lastname":"bar"}
  json_encode($component);

  //$user->date = new DateTime();

  /* Returns:
      {
          "firstname":"foo",
          "lastname":"bar",
          "birthdate": {
              "date":"2012-06-06 08:46:58",
              "timezone_type":3,
              "timezone":"Europe\/Berlin"
          }
      }
  */
  json_encode($user);
?>



<ul class="collection with-header">
  <li class="collection-header">
    <span class="header-item">BEMACASH VESTUÁRIO NFC-e + IMPRESSORA + GAVETA</span>
    <a href="#!" class="secondary-content header-value">
      <span class="value-content">R$ 299,00</span>
    </a>
    </a>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> Licenciamento Bemacash
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> Tablet Samsung Galaxy Tab E 9.6 SM-T560 199 100070
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> Suporte Metalico Tablet Bemacash 9.6 pol 499 100720
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> Gaveta de Dinheiro GD-56 PRETA 128000 100
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> Licença de Software Fiscal Manager
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
  <li class="collection-item">
    <div class="title">  
      <a>
        <i class="material-icons"> photo</i>
      </a> MP-4200 TH ETHERNET BR 10 1000830
      <a href="#!" class="secondary-content">
        <i class="material-icons">send</i>
      </a>
    </div>
  </li>
</ul>