# US-Project

User Story project  is a technical evaluation for the position - Front-end Web developer. 

### Installation 
Clone repository in your apache directory and run this command's:
```sh
$ cd us-project
$ npm install -d

```

### Build With

Dillinger uses a number of open source projects to work properly:

* [PHP] - Hypertext Preprocessor!
* [Materialize CSS] - responsive front-end framework based on Material Design
* [Gulp] - the streaming build system
* [jQuery] - duh


   [PHP]: <https://secure.php.net/>
   [Materialize CSS]: <http://materializecss.com/>
   [jQuery]: <http://jquery.com>
   [Gulp]: <http://gulpjs.com>
